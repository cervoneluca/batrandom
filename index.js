import Sentencer from 'sentencer';

const batRandom = (type = 'noun') => {
  switch (type) {
    case 'noun':
      return `bat-${Sentencer.make('{{ noun }}')}`;
    case 'adjective':
      return `bat-${Sentencer.make('{{ adjective }}')}`;
    case 'luca':
      return 'bat-luca';
    default:
      return `bat-${Sentencer.make('{{ noun }}')}`;
  }
};

export default batRandom;
